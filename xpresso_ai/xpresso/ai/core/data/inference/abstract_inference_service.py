""" Base object class for inference service """
import time
from os import environ, path
import json
from abc import abstractmethod
from flask import Flask, request, render_template
from flask.wrappers import BadRequest

from xpresso.ai.core.data.inference.generate_ui_form import GenerateDynamicUIForm
from xpresso.ai.core.data.versioning.controller_factory import \
    VersionControllerFactory
from xpresso.ai.core.commons.exceptions.xpr_exceptions import XprExceptions
from xpresso.ai.core.commons.utils.constants import OUTPUT_TYPE_FILES, ui_config_filename
from xpresso.ai.core.commons.utils.constants import DV_PROJECT_TOKEN, \
    PROJECT_TOKEN_IN_PROJECTS, PROJECT_NAME_IN_PROJECTS
from xpresso.ai.core.utils.api_utils import APIUtils

__all__ = ["AbstractInferenceService"]
__author = ["Sahil Malav"]


class AbstractInferenceService:

    def __init__(self):
        self.model = None
        self.api_utils = APIUtils()

    def load(self):
        """
        Fetches data from a data versioning repo using the args provided
        (commit id, repo name, etc.)
        Returns: model info

        """
        try:
            repo_name = environ['REPO_NAME']
            commit_id = environ['COMMIT_ID']
            branch_name = environ['BRANCH_NAME']
            dv_path = environ['DATA_VERSIONING_PATH']

            response_project = self.api_utils.get_project_info(
                {PROJECT_NAME_IN_PROJECTS: repo_name}
            )
            project_token = response_project[PROJECT_TOKEN_IN_PROJECTS]
            controller_factory = VersionControllerFactory(**{
                DV_PROJECT_TOKEN: project_token
            })
            version_controller = controller_factory.get_version_controller()

            commit_path = version_controller.pull_dataset(repo_name=repo_name,
                                                          xpresso_commit_id=commit_id,
                                                          branch_name=branch_name,
                                                          path=dv_path,
                                                          output_type=
                                                          OUTPUT_TYPE_FILES)
            model_path = commit_path + dv_path
            print("Waiting 10 seconds before starting the application...")
            time.sleep(10)
            self.load_model(model_path)
        except KeyError as e:
            faulty_key = e.args[0]
            print(f"Failed to fetch '{faulty_key}'.")
            raise XprExceptions("Failed to fetch the model.Exiting.")
        except XprExceptions as e:
            print(f"Failed to fetch the model from data versioning repository."
                  f"\nDetails: {e.message}")
            raise XprExceptions("Exiting failed to fetch the model")

    @abstractmethod
    def load_model(self, model_path):
        """
        Initialises model. Must be overridden by the user.
        Args:
            model_path (str): path where model is stored on local disk

        Returns: Loaded model.

        """

    @abstractmethod
    def transform_input(self, input_request):
        """
        Does any required transformations on the input request.
        Must be overridden by the user.
        Args:
            input_request: Input request

        Returns (dict): transformed input

        """

    @abstractmethod
    def transform_output(self, output_response):
        """
        Does any required transformations on the output response.
        Must be overridden by the user.
        Args:
            output_response: Output response

        Returns (dict): transformed output
        """

    @abstractmethod
    def predict(self, input_request):
        """
        Does prediction based on the input request and the model provided.
        Must be overridden by the user.
        Args:
            input_request: Input request

        Returns (dict): Predicted output

        """

    @staticmethod
    def check_incoming_request():
        input_request = {}
        try:
            if request.method == 'POST' and request.is_json:
                input_request = request.get_json()
            if request.method == 'POST' and request.form:
                form_data = request.form
                input_request['input'] = form_data.to_dict()

        except (ValueError, TypeError, BadRequest, json.JSONDecodeError) as e:
            print(e)
        if not input_request:
            print("ERROR! Your request couldn't be parsed. "
                  "Make sure that it's a valid JSON object.")
        return input_request

    def return_transformed_output(self, request_json):
        if request_json:
            input_request = request_json
        else:
            input_request = self.check_incoming_request()

        if 'input' not in input_request:
            return {"message": "Input data not provided"}, 400

        run_name = environ['RUN_NAME']
        try:
            predict_request = self.transform_input(input_request['input'])
            output_response = self.predict(predict_request)
            transformed_output = self.transform_output(output_response)
        except XprExceptions as e:
            return {"message": e.message, "run_name": run_name}, 400
        return {"message": "success", "run_name": run_name,
                "results": transformed_output}, 200

    def run_api(self, port):
        """
        Starts the API with "/predict" route, which calls transform_input,
        predict and transform_output methods.
        Args:
            port: Port to run the flask server on

        Returns: Nothing

        """
        xpresso_location = environ['XPRESSO_PACKAGE_PATH']
        inference_location = 'xpresso/ai/core/data/inference/'
        app = Flask(self.__class__.__name__, template_folder=
        path.join(xpresso_location, inference_location, 'templates'),
                    static_folder=path.join(xpresso_location, inference_location, 'static'))
        app.secret_key = "s;djnf2DSFKAL23))$&)@LJSDO@_!@#!_DSAD__#%*_!@_#"

        @app.route("/", methods=["GET"])
        def return_dynamic_ui():
            """
            Returns: Dynamic UI based on UI configuration provided

            """
            abstract_inference_service = AbstractInferenceService()
            ui_form_response = abstract_inference_service.generate_form()
            if ui_form_response:
                return render_template('inference_ui.jinja2',
                                       form=ui_form_response['template_obj'],
                                       ui_config=zip(ui_form_response['label_list'], ui_form_response['feature_list']),
                                       project_name=environ['REPO_NAME'],
                                       template='form-template')
            if not ui_form_response:
                return render_template('default_ui.jinja2')

        @app.route("/predict_form", methods=["POST"])
        def predict():
            try:
                input_request = self.check_incoming_request()
                ui_config = environ['UI_CONFIG_FILEPATH']
                with open(ui_config) as f:
                    ui_configuration = json.load(f)
                for (input_key, input_value, ui_config) in zip(input_request['input'].keys(),
                                                               input_request['input'].values(),
                                                               ui_configuration['fields']):
                    if input_key == ui_config['name']:
                        key_datatype = ui_config['data_type']
                        if key_datatype == "integer":
                            input_request['input'][input_key] = int(input_value)
                        if key_datatype == "float":
                            input_request['input'][input_key] = float(input_value)
                        if key_datatype == "string":
                            input_request['input'][input_key] = str(input_value)
                transformed_output = self.return_transformed_output(input_request)
                abstract_inference_service = AbstractInferenceService()
                ui_form_response = abstract_inference_service.generate_form()
                ui_form_response['template_obj'].predicted_result.data = transformed_output
                ui_form_response['feature_list'].append('predicted_result')
                ui_form_response['label_list'].append('Predicted Result')
                return render_template('inference_ui.jinja2',
                                       form=ui_form_response['template_obj'],
                                       ui_config=zip(ui_form_response['label_list'], ui_form_response['feature_list']),
                                       project_name=environ['REPO_NAME'],
                                       template='form-template')

            except XprExceptions as e:
                return {"message": e.message, "run_name": environ['RUN_NAME']}, 400

        @app.route("/reload", methods=["POST"])
        def reload_dynamic_ui():
            try:
                file_path = request.form['filename']
                abstract_inference_service = AbstractInferenceService()
                ui_form_response = abstract_inference_service.generate_form(file_path)
                if not ui_form_response:
                    return render_template('default_ui.jinja2')
                return render_template('inference_ui.jinja2',
                                       form=ui_form_response['template_obj'],
                                       ui_config=zip(ui_form_response['label_list'], ui_form_response['feature_list']),
                                       project_name=environ['REPO_NAME'],
                                       template='form-template')

            except XprExceptions as e:
                raise e

        @app.route("/predict", methods=["POST"])
        def post():
            """
            Calls transform and predict methods.
            Returns: Inference output.

            """
            run_name = environ['RUN_NAME']
            try:
                transformed_output = self.return_transformed_output()
            except XprExceptions as e:
                return {"message": e.message, "run_name": run_name}, 400
            return transformed_output

        app.run(debug=False, port=str(port), host='0.0.0.0',
                use_reloader=False)

    @abstractmethod
    def report_inference_status(self, service_info, status):
        """
        Reports live inference status.
        Args:
            service_info: information required to fetch inference svc
             from database
            status: status to report
        Returns: Nothing. Updates status in database.

        """
        # ToDo : after integrating with Experiment Manager

    def fetch_version_controller(self, project_name):
        """
              generates new instance of version controller for data versioning
         using project_token fetched using project_name

        Args:
            project_name: name of the project
        Returns:
             returns version controller object
        """
        response_project = self.api_utils.get_project_info(
            {PROJECT_NAME_IN_PROJECTS: project_name}
        )
        project_token = response_project[PROJECT_TOKEN_IN_PROJECTS]
        controller_factory = VersionControllerFactory(**{
            DV_PROJECT_TOKEN: project_token
        })
        version_controller = controller_factory.get_version_controller()
        return version_controller

    def generate_form(self, filepath=None):
        """
        Generates dynamic UI based on the configuration provided in the file
        Args:
            filepath: (optional) used in case when user wants to reload
            UI based on file path provided
        Returns:

        """
        try:
            file_path = path.join('data', ui_config_filename)
            if not filepath and path.exists(file_path):
                environ['UI_CONFIG_FILEPATH'] = file_path
            elif filepath and path.exists(filepath):
                environ['UI_CONFIG_FILEPATH'] = filepath
            elif filepath and not path.exists(filepath):
                return None
            elif not filepath and not path.exists(file_path):
                return None
            with open(environ['UI_CONFIG_FILEPATH']) as f:
                ui_configuration = json.load(f)
            generate_ui_form = GenerateDynamicUIForm(ui_configuration)
            ui_label_list, ui_feature_list, ui_form = generate_ui_form.generate_ui()
            return {"label_list": ui_label_list, "feature_list": ui_feature_list, "template_obj": ui_form}
        except (XprExceptions, FileNotFoundError) as err:
            raise err
