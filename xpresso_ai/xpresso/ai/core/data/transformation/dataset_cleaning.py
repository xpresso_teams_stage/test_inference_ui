""" Module for Data Transformation in a dataset """

__author__ = 'Shlok Chaudhari'
__all__ = ['DataCleaning']

import pandas as pd
import functools
import xpresso.ai.core.commons.utils.constants as constants


class DataCleaning:
    """

    This class is used to transform a dataset as per the user.
    It extends the AbstractTransformer class which a base class
    for all the classes dealing with data transformation

    """

    def __init__(self, dataset):
        self.dataset = dataset

    def start(self, deduplicate=False,
              columns=None,
              keep=True,
              clean_dates=False,
              date_format="%Y-%m-%d"):

        self.transform(deduplicate=deduplicate,
                       columns=columns,
                       keep=keep,
                       clean_dates=clean_dates,
                       date_format=date_format)

    def transform(self, deduplicate=False,
                  columns=None,
                  keep=True,
                  clean_dates=False,
                  date_format="%Y-%m-%d"):
        """

        Transforms dataset according to the parameters specified
        in the inputs

        Args:
            deduplicate (bool): used to deduplicate multiple rows

            columns (list): list of certain columns considered
            for identifying duplicates, by default use all of the columns

            keep (bool): keep one of the duplicate rows or remove all,
            default is True
                - True: Drop all except one
                - False: Drop all duplicates

            clean_dates (bool): used to clean date format of entire
            column.

            date_format (str): Date format specified by the
            user. Default is 'YYYY-MM-DD'

        Returns:
            (DataFrame): an object which has undergone transformation

        """

        if deduplicate is True:
            self.de_duplicate(self.dataset.data, columns, keep)

        if clean_dates is True:
            self.format_all_datetype(date_format)

    @staticmethod
    def de_duplicate(data, columns, keep):
        """

        Dropping duplicate rows with respect to the parameters provided

        Args:
            data (DataFrame): dataset needed for deduplication

            columns (String): list of certain columns considered
            for identifying duplicates, by default use all of the columns

            keep (bool): keep one of the duplicate rows or remove all,
            default is True
                - True: Drop all except one
                - False: Drop all duplicates

        Returns:
            (DataFrame): dataframe with one row of the multiple
            rows present in the data

        """

        if columns is not None:
            if functools.reduce(lambda i, j: i and j,
                                map(lambda m, k: m == k, columns, data.columns),
                                False):
                columns = None
        if keep is False:
            pd.DataFrame.drop_duplicates(
                data, subset=columns, keep=False, inplace=True)
        else:
            pd.DataFrame.drop_duplicates(
                data, subset=columns, keep="first", inplace=True)

    def format_all_datetype(self, date_format):
        """

        Method to format all date type columns from the given dataset

        Args:
            date_format (String): Date format specified by the
            user. Default is 'YYYY-MM-DD'

        """

        datetype_columns = [attr.name
                            for attr in self.dataset.info.attributeInfo
                            if attr.type is constants.date]
        for column in datetype_columns:
            new_dates = self.format_datetype(
                self.dataset.data[column], date_format)
            dates_df = pd.DataFrame(new_dates, columns=[column])
            self.dataset.data.update(dates_df)

    @staticmethod
    def format_datetype(data, date_format):
        """

        Cleaning dataset containing date attribute. Regularizing the
        format in one standard form specified by the user. Default is

        Args:
            date_format (String): Date format specified by the user
            Default is 'YYYY-MM-DD'

            data (list): records of date attribute

        Returns:
            (DataFrame): an object with one standard date
                    format

        """

        if ("%Y" and "%m" and "%d") not in date_format:
            date_format = "%Y-%m-%d"
        dates = pd.to_datetime(data, infer_datetime_format=True).apply(
            lambda x:
            x.strftime(date_format) if not pd.isnull(x) else '')

        return dates
