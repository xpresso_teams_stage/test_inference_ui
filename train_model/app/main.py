"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
import os
import sys

import pandas as pd
import tensorflow as tf
import tensorflow.compat.v1.keras.backend as K
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Input, Embedding, Concatenate, Dense, \
    Flatten, Reshape, BatchNormalization, Dropout

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.INFO)

DEFAULT_BATCH_SIZE = 128
DEFAULT_TRAIN_EPOCHS = 10
# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Mrunalini Dhapodkar"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="train_model",
                   level=logging.INFO)


class LossAndErrorPrintingCallback(tf.keras.callbacks.Callback):
    """ This block sets up a hook into the Keras library, which enables
    the status to be reported back to the Controller at the end of each epoch. """

    def __init__(self, dnn_training_object):
        super().__init__()
        self.dnn_obj = dnn_training_object

    def on_epoch_end(self, epoch, logs=None):
        """ This is called at the end of every epoch. In this logs
        we can get different metrics that were recorded.
        We are sending the required petrics to the xpresso controller"""

        logging.info(
            'The average loss for epoch {} is {:7.2f}.'.format(epoch,
                                                               logs['loss']))
        logging.info(logs)
        report_status = {
            "status": {
                "status": "Epoch {}".format(epoch)
            },
            "metric": {
                "loss": logs["loss"],
                "epoch": epoch
            }
        }
        logging.info(report_status)
        self.dnn_obj.report_status(status=report_status)


class TrainModel(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, xpresso_run_name, parameters_filename,
                 parameters_commit_id, base_folder="/data/sales_data"):
        super().__init__(name="TrainModel",
                         run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)
        # if you have specified parameters_filename or parameters_commit_id,
        # the run parameters can be accessed from self.run_parameters
        """ Initialize all the required constants and data here """
        self.base_folder = base_folder
        self.split_train_path = os.path.join(self.base_folder,
                                             "split_train.csv")
        self.split_valid_path = os.path.join(self.base_folder,
                                             "split_valid.csv")
        self.orig_train = None

        self.train = None
        self.valid = None
        self.feature_names = None
        self.csv_defaults = None

    def load(self):
        """ Load precessed dataset into a member variable"""
        self.orig_train = pd.read_csv(
            os.path.join(self.base_folder,
                         "dnn_combined_train.csv"))

    def build_vocabulary(self, df: pd.DataFrame, cols):
        """ Iterates through each columns and build the vocabulary
        for encoding
        Returns:
            dict: vocabulary dictionary
        """
        vocab = {}
        for col in cols:
            values = df[col].unique()
            col_type = type([x for x in values if x is not None][0])
            default_value = col_type()
            vocab[col] = sorted(values, key=lambda x: x or default_value)
        return vocab

    def cast_columns(self, df: pd.DataFrame, cols):
        """ Type cast columns into float

         Returns:
            pandas.Dataframe
        """
        for col in cols:
            df[col] = df[col].astype(float)
        return df

    def lookup_columns(self, df, vocab):
        """ Create vocabulary dictionary reverse lookup for each columns

        Returns:
            pandas.Dataframe
        """
        for col, mapping in vocab.items():
            df[col] = df[col].apply(mapping.index)
            df[col] = df[col].astype(float)
        return df

    def exp_rmspe(self, y_true, y_pred):
        """Competition evaluation metric RMSPE. It expects logarthmic inputs.
        Returns:
            float: Root Mean Square percentage error
        """
        pct = tf.square((tf.exp(y_true) - tf.exp(y_pred)) / tf.exp(y_true))
        # Compute mean excluding stores with zero denominator.
        x = tf.reduce_sum(tf.where(y_true > 0.001, pct, tf.zeros_like(pct)))
        y = tf.reduce_sum(
            tf.where(y_true > 0.001, tf.ones_like(pct), tf.zeros_like(pct)))
        return tf.sqrt(x / y)

    def act_sigmoid_scaled(self, x):
        """Sigmoid scaled to logarithm of maximum sales scaled by 20%."""
        return tf.nn.sigmoid(x) * tf.compat.v1.log(42000.00) * 1.2

    def start(self, xpresso_run_name, batch_size=DEFAULT_BATCH_SIZE, train_eopchs=DEFAULT_TRAIN_EPOCHS):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
            identify the current run. It must be passed. While running as
            pipeline, xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=xpresso_run_name)
            self.load()
            self.batch_size = batch_size
            self.train_eopchs = train_eopchs
            logging.info("preparing tensor")
            categorical_cols = ['Store', 'DayOfWeek', 'Promo', 'StateHoliday',
                                'SchoolHoliday', 'StoreType', 'Assortment',
                                'Promo2', 'Day', 'Month', 'Year', 'isCompetition']

            numeric_cols = ['CompetitionDistance']

            all_cols = categorical_cols + numeric_cols
            CUSTOM_OBJECTS = {'exp_rmspe': self.exp_rmspe,
                              'act_sigmoid_scaled': self.act_sigmoid_scaled}

            self.max_sales = float(self.orig_train['Sales'].max())
            vocab = self.build_vocabulary(self.orig_train[categorical_cols],
                                          categorical_cols)
            self.orig_train = self.cast_columns(self.orig_train,
                                                numeric_cols + ['Sales'])
            self.orig_train = self.lookup_columns(self.orig_train, vocab)

            train, valid = train_test_split(self.orig_train, test_size=0.05,
                                            random_state=42)
            train.to_csv("/data/sales_data/split_train.csv")
            valid.to_csv("/data/sales_data/split_valid.csv")
            # train = pd.read_csv("/data/sales_data/split_train.csv")
            # valid = pd.read_csv("/data/sales_data/split_valid.csv")
            logging.info(train.head())
            logging.info(valid.head())
            logging.info("Starting session")
            config = tf.compat.v1.ConfigProto(device_count={'GPU': 0})
            K.set_session(tf.compat.v1.Session(config=config))

            # Build the model.
            logging.info("Building model")
            inputs = {col: Input(shape=(1,), name=col) for col in all_cols}
            embeddings = [
                Embedding(len(vocab[col]), 10, input_length=1, name='emb_' + col)(
                    inputs[col])
                for col in categorical_cols]
            continuous_bn = Reshape((1, 1), name='reshape_' + numeric_cols[0])(
                inputs[numeric_cols[0]])
            continuous_bn = BatchNormalization()(continuous_bn)
            x = Concatenate()(embeddings + [continuous_bn])
            x = Flatten()(x)
            x = Dense(1000, activation='relu',
                      kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
            x = Dense(1000, activation='relu',
                      kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
            x = Dense(1000, activation='relu',
                      kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
            x = Dense(500, activation='relu',
                      kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
            x = Dropout(0.5)(x)
            output = Dense(1, activation=self.act_sigmoid_scaled)(x)
            model = tf.keras.Model([inputs[f] for f in all_cols], output)
            model.summary()
            opt = tf.keras.optimizers.Adam(lr=1e-4, epsilon=1e-3)
            model.compile(opt, 'mae', metrics=[self.exp_rmspe])

            logging.info("Training model")
            history = model.fit(
                x=[tf.convert_to_tensor(train[col].values) for col in all_cols],
                y=[tf.convert_to_tensor(train['Sales'].values)],
                callbacks=[LossAndErrorPrintingCallback(self)],
                verbose=2,
                batch_size=self.batch_size,
                validation_steps=len(valid) / self.batch_size,
                steps_per_epoch=len(train) / self.batch_size,
                epochs=self.train_eopchs,
                validation_data=
                ([tf.convert_to_tensor(valid[col].values) for col in all_cols],
                 [tf.convert_to_tensor(valid['Sales'].values)]))
            logging.info(history)
            logging.info("Saving model")
            if not os.path.exists(self.OUTPUT_DIR):
                os.makedirs(self.OUTPUT_DIR)
            model.save(os.path.join(self.OUTPUT_DIR, 'saved_model.h5'))
            logging.info("Saved model... load model")

            super().completed(push_exp=True)

        except Exception as err:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
            self.completed(success=True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    run_name = ""
    params_filename = None
    params_commit_id = None
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    if len(sys.argv) >= 4:
        params_filename = sys.argv[2] if sys.argv[2] != "None" else None
        params_commit_id = sys.argv[3] if sys.argv[3] != "None" else None

    pipeline_job = TrainModel(run_name, params_filename, params_commit_id)
    pipeline_job.start(xpresso_run_name=run_name)
