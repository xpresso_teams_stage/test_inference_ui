"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
import os
import pandas as pd
import tensorflow as tf

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Mrunalini Dhapodkar"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="inference",
                   level=logging.INFO)


class Inference(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """
        categorical_cols = ['Store', 'DayOfWeek', 'Promo', 'StateHoliday',
                            'SchoolHoliday', 'StoreType', 'Assortment',
                            'Promo2', 'Day', 'Month', 'Year', 'isCompetition']

        numeric_cols = ['CompetitionDistance']
        self.all_cols = categorical_cols + numeric_cols

    def exp_rmspe(self, y_true, y_pred):
        """Competition evaluation metric, expects logarthmic inputs."""
        pct = tf.square((tf.exp(y_true) - tf.exp(y_pred)) / tf.exp(y_true))
        # Compute mean excluding stores with zero denominator.
        x = tf.reduce_sum(tf.where(y_true > 0.001, pct, tf.zeros_like(pct)))
        y = tf.reduce_sum(
            tf.where(y_true > 0.001, tf.ones_like(pct), tf.zeros_like(pct)))
        return tf.sqrt(x / y)

    def act_sigmoid_scaled(self, x):
        """Sigmoid scaled to logarithm of maximum sales scaled by 20%."""
        return tf.nn.sigmoid(x) * tf.compat.v1.log(42000.00) * 1.2

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        CUSTOM_OBJECTS = {'exp_rmspe': self.exp_rmspe,
                          'act_sigmoid_scaled': self.act_sigmoid_scaled}
        self.model = tf.keras.models.load_model(os.path.join(model_path, "saved_model.h5"),
                                                custom_objects=CUSTOM_OBJECTS)

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        data_to_list = [input_request[col] for col in self.all_cols]
        logging.info(data_to_list)
        feature = pd.DataFrame(data=[data_to_list],
                               columns=self.all_cols)
        logging.info([feature[col].values for col in self.all_cols])
        return feature

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        tf_vector = [tf.convert_to_tensor(input_request[col].values) for col in
                     self.all_cols]
        value = self.model.predict(tf_vector)
        logging.info(value)
        logging.info(type(value[0]))
        return value[0].tolist()

    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        return output_response


if __name__ == "__main__":
    pred = Inference()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
